(function (undefined) {
	const AppKernel = require('../AppKernel');

	var ContainerAware;

	ContainerAware = {
		getContainer: function (containerName) {
			return AppKernel.getContainer(containerName);
		}
	};

	module.exports = ContainerAware;
}());