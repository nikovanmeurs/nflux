/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {
    const
        _             = require('lodash'),
        RequestMethod = require('./RequestMethod');

    var JsonApi;

    /**
     * Generic JsonApi class, exposing functions for the GET, POST, PUT and DELETE request methods
     * @class
     */
    JsonApi = {

        /**
         * Makes a request with the DELETE method
         * @param {String} url
         * @param {Object} payload
         * @param {Object} [headers]
         */
        delete: function (url, headers) {
            return sendRequest(url, [], RequestMethod.DELETE, headers);
        },

        /**
         * Makes a request with the GET method
         * @param {String} url
         * @param {Object} [headers]
         */
        get: function (url, headers) {
            return sendRequest(url, null, RequestMethod.GET, headers);
        },

        /**
         * Makes a request with the POST method
         * @param {String} url
         * @param {Object} payload
         * @param {Object} [headers]
         */
        post: function (url, payload, headers) {
            return sendRequest(url, payload, RequestMethod.POST, headers);
        },

        /**
         * Makes a request with the PUT method
         * @param {String} url
         * @param {Object} payload
         * @param {Object} [headers]
         */
        put: function (url, payload, headers) {
            return sendRequest(url, payload, RequestMethod.PUT, headers);
        }
    };

    // Event handlers, triggers and other private functions
    // Declared as function statements,
    // classes consuming the Store don't have anything to do with these

    /**
     * Sends an XMLHttpRequest and returns a Promise
     * @param {String} url
     * @param {Object} [payload]
     * @param {String} method
     * @param {Object} [headers]
     * @returns {Promise}
     */
    function sendRequest (url, payload, method, headers) {

        headers = headers || {};

        console.log(url);
        console.log(method);

        return new Promise (function (resolve, reject) {
            var request = new XMLHttpRequest(),
                requestBody;

            request.open(method, url, true);

            // Resolves the Promise if response is OK,
            // rejects the Promise otherwise
            request.onload = function () {

                if (199 < request.status && request.status < 300 ) {
                    var responseText    = request.response,
                        body            = responseText ? JSON.parse(responseText) : '';

                    resolve(body);

                } else {

                    reject(Error(request.statusText));
                }
            };

            // Rejects the Promise
            request.onerror = function () {

                reject(Error("Network- or CORS Error"));
            };

            // Add a request body if payload is present
            // Converts the payload to JSON if payload is not of type string
            if (payload) {

                requestBody = (payload instanceof String) ? payload : JSON.stringify(payload);
                headers['Content-type'] = 'application/json';
            }

            setHeaders(request, headers);

            request.send(requestBody);
        });
    }

    /**
     * Attaches the headers to the request
     * @param {XMLHttpRequest} request
     * @param {Object} headers
     */
    function setHeaders(request, headers) {

        // set headers, pass request as scope
        _.forEach(headers, setHeader, request);
    }

    /**
     * Attaches a header to the request object,
     * Note: the request object needs to be set as the method's scope
     * @param {String} value
     * @param {String} key
     */
    function setHeader (value, key) {

        this.setRequestHeader(key, value);
    }

    module.exports = JsonApi;
}());