(function () {
    
    const
        _               = require('lodash'),
        React           = require('react'),
        ContainerAware  = require('../core/container/ContainerAware');

    var shouldComponentUpdate;

    shouldComponentUpdate = function (nextProps, nextState) {
        return !(_.isEqual(this.props, nextProps) && _.isEqual(this.state, nextState));
    }

    module.exports = function () {

        var args,
            prototype;

        args        = [].slice.call(arguments, 0);

        // Add mixins and functions which should be present by default
        // Unshift to make sure these can be overwritten
        // by the prototype definitions in arguments
        args.unshift(ContainerAware);
        args.unshift({ shouldComponentUpdate: shouldComponentUpdate });

        // Merge args to single object
        prototype   = _.merge.apply(_, args);

        return React.createClass(prototype);
    };
}());