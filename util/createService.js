/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function () {

    const
        AbstractService = require('../service/AbstractService'),
        createClass     = require('./createClass');

    var createService;

    createService = function (moduleName, properties) {

        properties.name = moduleName;

        return createClass(AbstractService, properties);
    };

    module.exports = createService;
}());
