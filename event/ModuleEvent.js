/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {

    const
        createEvent = require('../util/createEvent');

    var ModuleEvent;

    /**
     * Triggered by the AppKernel when a module is started/stopped
     */
    ModuleEvent = createEvent(
        [
            'module-start',
            'module-started',
            'module-stop',
            'module-stopped',
        ],
        function (eventName, moduleName) {
            this.emit(eventName, moduleName);
        }
    );

    module.exports = ModuleEvent;
}());