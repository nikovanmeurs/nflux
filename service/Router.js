/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
 (function (undefined) {
    const
        _                   = require('lodash'),
        AbstractService     = require('./AbstractService'),
        RouteEvent          = require('../event/RouteEvent'),
        createClass         = require('../util/createClass'),
        stringFromTemplate  = require('../util/stringFromTemplate');

    var Router,
        _hostName,
        _instance,
        _port,
        _routes;

    /**
     * Routes requests to the right page
     * @constructor
     */
    Router = createClass(AbstractService, {

        /**
         * Initializes the Router with sane defaults
         */
        construct: function () {
            _routes = {};
            _hostName = 'localhost';
            _port = 80;
        },

        name: 'router',

        add: function (route) {

            var routeName = route.name;

            if(_routes[routeName]) {
                throw new Error(stringFromTemplate('A route with name {name} already exists', {
                    name: routeName
                }));
            }
            _routes[routeName] = route;
        },

        /**
         * Sets the hostname
         * @param {String} hostName
         * @returns {Router}
         */
        setHostName: function (hostName) {
            _hostName = hostName;

            return this;
        },

        /**
         * Sets the port
         * @param {String} port
         * @returns {Router}
         */
        setPort: function (port) {
            _port = port;

            return this;
        },

        /**
         * Replaces all routes
         * @param {Array} routes
         * @returns {Router}
         */
        setRoutes: function (routes) {

            _routes = {};
            routes.forEach(this.add);

            return this;
        },

        /**
         * Routes to a given url
         * @param {String} url
         * @returns {Router}
         */
        route: function (url) {

            console.log('route');

            var matchedRoute = match(getPath(url));

            console.log(matchedRoute);

            if ('object' === typeof matchedRoute) {
                RouteEvent.trigger(RouteEvent.MATCHED, matchedRoute);
            }

            return this;
        },

        /**
         * Implementation of AbstractService.start, binds event listeners
         * @returns {Router}
         */
        start: function () {
            RouteEvent
                .on(RouteEvent.ADD, handleRouteAdd)
                .on(RouteEvent.ROUTE, handleRoute);

            return this;
        },

        /**
         * Implementation of AbstractService.stop, unbinds event listeners
         * @returns {Router}
         */
        stop: function () {
            RouteEvent
                .off(RouteEvent.ADD, handleRouteAdd)
                .off(RouteEvent.ROUTE, handleRoute);

            return this;
        }
    });

    // Event handlers, triggers and other private functions
    // Declared as function statements,
    // classes consuming the module don't have anything to do with these

    /**
     * Strips the domain from a url, returning only the path
     * @param {String} url
     * @returns {String}
     */
    function getPath (url) {
        var template = '(http){1}s?:\/\/{host}(:{port})?',
            regexString,
            regex;

        regexString = template.replace('{host}', _hostName).replace('{port}', _port);
        regex = new RegExp(regexString, 'g');

        return url.replace(regex, '');
    }

    /**
     * Responds to RouteEvent.ROUTE
     * @param {String} url
     */
    function handleRoute (url) {
        console.log('handleRoute');
        _instance.route(url)
    }

    /**
     * Responds to RouteEvent.ADD
     * @param {String} url
     */
    function handleRouteAdd (route) {
        _instance.add(route);
    }

    /**
     * Matches a path against the registered routes
     * @param {String} path
     * @returns {Object}
     */
    function match (path) {

        var matchedRoute,
            params;

        console.log(path);
        console.log(_routes);

        _.each(_routes, function (route) {

            var routeTemplate = route.route.slice(0),
                match;

            _.each(route.requirements, function (value, key) {

                var regexp = new RegExp('\\{' + key + '\\}');
                routeTemplate = routeTemplate.replace(regexp, '(' + value + ')');
            });

            match = path.match(routeTemplate);

            if (null != match) {

                match.shift();

                matchedRoute = route;
                params = {};

                // map matched values to requirements keys
                _.each(Object.keys(route.requirements || []), function (key, index) {

                    var value = match[index].slice(0);

                    if ('\\d+' === route.requirements[key]) {
                        value = parseInt(value, 10);
                    }

                    params[key] = value;
                });

                return false;
            };
        });

        // Return everything except route definition and requirements
        if (matchedRoute) {
            return _.merge(
                {
                    parameters: params,
                    request: path
                },
                _.omit(matchedRoute, ['route', 'requirements'])
            );
        }
    }

    // Instantiate the Router, to be consumed as singleton instance
    _instance = new Router();

    // Export the instance
    module.exports = _instance;
 }());
