(function (undefined) {

    // Use a SEAF to ensure each ServiceContainer instance has a unique private services object
    module.exports = (function () {

        var ServiceContainer,
            _services;

        ServiceContainer = function () {
            _services = {};
        };

        ServiceContainer.prototype = {

            add: function (serviceName, service) {

                _services[serviceName] = service;

                return this;
            },

            get: function (serviceName) {

                if (!_services[serviceName]) {
                    message = 'Service {name} is not registered';
                    throw new Error(message.replace(/\{name\}/g, serviceName));
                }

                return _services[serviceName];
            }
        }

        return ServiceContainer;

    }());
}());