# Nflux

Nflux is a flux implementation similar to Reflux. It differs however in the fact that it does away with the central dispatcher and actions. Instead, communication between stores and views is decoupled by events. In contrast to most event systems the events are not emitted on either store or view, but on the EventClass itself.

## Installation

Run `npm install git+ssh://git@bitbucket.org:nikovanmeurs/nflux.git --save`

## Usage

An event class can be created as follows:


```
#!javascript

TodoEvent = Nflux.createEvent([
    'todo-add',
    'todo-update',
    'todo-remove',
    'todo-list',
    'todo-listed',
    'todo-changed'
]);
```

A store can be created as follows:


```
#!javascript

Nflux.createStore('todo', TodoEvent);
```

The TodoEvent class can then be included in the view definition and used to listen to changes in the store:

```
#!javascript

TodoEvent.on(TodoEvent.CHANGED, (todos) => this.setState({ todos: todos }));
```

### Dependency injection

Nflux contains a dependency injection mechanism similar to Symfony II's service container. A default container is registered with the AppKernel on initialisation and can be utilised by extending or mixing in the abstract ContainerAware class.

Nflux contains a generator function for React components which does exactly this and adds a sane shouldComponentUpdate function as bonus. The generator function can be used as follows:

```
#!javascript

var MyComponent = Nflux.createView(props);
```

### Routing

Nflux also contains a Symfony II inspired Router. The router can either be initialised with a set of predefined routes by calling `Router.setRoutes(routes)` or the routing table can be built dynamically by calling `RouteEvent.trigger(RouteEvent.ADD, routeDefinition);`.

A routing definition can look as follows:


```
#!javascript

{
    name: 'todo-show',
    route: '^/todos/{id}',
    requirements: {
        id: '\\d+'
    },
    endpoint: MyComponent
}
```

### Initialisation

Using Nflux, your application bootstrap file might look as follows:


```
#!javascript

(function (window, undefined) {

    const
        Nflux  = require('nflux'),
        React  = require('react'),
        routes = require('./config/routes.js');

    var TodoApp,
        TodoEvent,
        TodoStore,
        kernel,
        Router;

    kernel = Nflux.kernel;
    Router = Nflux.service.Router;

    // Configure the Router
    Router
        .setHostName(window.location.hostname)
        .setPort(window.location.port)
        .setRoutes(routes);

    // Starting the service binds event listeners to the service definition
    // subsequently, calling kernel.stopService unbinds event listeners from the service definition
    kernel
        .startService(Router.name)

    // Make sure the router module is started before requiring the top-level view
    // when adding routes using RouteEvent.ADD
    TodoApp = require('./view/TodoApp');

    React.renderComponent(TodoApp, document.body);

    Router.route(window.location.href);

}(window));
```