/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function () {

    const _ = require('lodash');

    /**
     * Creates a string by replacing the params in template
     * @param {String} template
     * @param {Object} params
     * returns {String}
     */
    function stringFromTemplate (template, params) {

        return _.reduce(params, replace, template);
    }

    /**
     * Replaces {needle} with value in haystack
     * @param {String} haystack
     * @param {*} value
     * @param {String} needle
     * @returns {String}
     */
    function replace (haystack, value, needle) {
        return haystack.replace(new RegExp('\{' + needle + '\}'), value);
    }

    module.exports = stringFromTemplate;
}());
