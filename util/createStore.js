/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function () {

    const
        _   = require('lodash');

    /**
     * Utility function to create an EntityStore
     * @param {String} entityType should be singular
     * @param {Object} EventClass should be created through createEvent
     * @deprecated since version 1.1.0, will be removed in 2.0.0
     */
    function createStore (entityType, EventClass) {

        return (function () {

            var _entities = [],
                Store;

            /**
             * @constructor
             */
            Store = {

                /**
                 * Adds an entity to the Store
                 * @param {Object} entity
                 * @returns {Store}
                 */
                add: function (entity) {

                    return Store.addAll([entity]);
                },

                /**
                 * Adds multiple entities to the Store
                 * @param {Array} entities
                 * @returns {Store}
                 */
                addAll: function (entities) {

                    _entities = _entities.concat(entities);

                    triggerChanged();

                    return Store;
                },

                /**
                 * Returns all entities for which predicate is truthy
                 * @param  {Function|Object} predicate
                 * @return {Array}
                 */
                find: function (predicate) {

                    return _.where(_entities, predicate);
                },

                /**
                 * Returns all entities
                 * @returns {Object}
                 */
                findAll: function () {

                    return _entities;
                },

                /**
                 * Returns the first entity for which predicate is truthy
                 * @param {Function|Object} predicate
                 * @return {Object}
                 */
                findOne: function (predicate) {

                    return _.find(_entities, predicate);
                },

                /**
                 * Returns the entity with id = id
                 * @param  {Number|String} id
                 * @return {Object}
                 */
                findOneById: function (id) {

                    return Store.findOne({ id: id });
                },

                /**
                 * Removes entity from the store
                 * @param  {Object} entity
                 */
                remove: function (entity) {

                    _entities = _.reject(_entities, entity);

                    triggerChanged();

                    return Store;
                },

                /**
                 * Replaces a single entity in the Store
                 * @param {Number|String} id
                 * @param {Object} newEntity
                 * @return Store
                 */
                replace: function (id, newEntity) {

                    var index = _.findIndex(_entities, { id: id });

                    _entities.splice(index, 1, newEntity);

                    triggerChanged();
                    triggerListed();

                    return Store;
                },

                /**
                 * Replaces all entities in the Store
                 * @param {Array} entities
                 * @return Store
                 */
                replaceAll: function (entities) {

                    _entities = entities;

                    triggerChanged();
                    triggerListed();

                    return Store;
                },

                /**
                 * Modifies an entity according to the diff
                 * @param {Number} id
                 * @param {Update} update
                 * @returns Store
                 */
                update: function (diff) {

                    var entity,
                        entityId,
                        newEntity;

                    entityId = diff.id;
                    entity   = Store.findOneById(entityId);

                    newEntity = Object.create(entity, diff);

                    return Store.replace(entityId, newEntity);
                }
            };

            // Event handlers, triggers and other private functions
            // Declared as function statements,
            // classes consuming the Store don't have anything to do with these

            /**
             * Triggers EventClass.CHANGE
             * @param {Object} entity
             */
            function triggerChanged () {

                EventClass.trigger(EventClass.CHANGED, Store.findAll());
            }

            /**
             * Triggers EventClass.LISTED
             * @see Store.findAll
             */
            function triggerListed () {

                // Passes all entities as event data
                EventClass.trigger(EventClass.LISTED, Store.findAll());
            }

            // Event binding
            EventClass
                // .on(EventClass.ADD, handleAdd)
                .on(EventClass.CREATE, Store.add.bind(Store))
                .on(EventClass.LIST, triggerListed)
                .on(EventClass.UPDATE, Store.update.bind(Store))
                .on(EventClass.REMOVE, Store.remove.bind(Store));

            if (EventClass.SET) {
                EventClass.on(EventClass.SET, Store.replaceAll.bind(Store));
            }

            return Store;
        }());
    }

    module.exports = createStore;
}());
