/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {

    /**
     * Enum for request methods
     * @readonly
     * @enum {String}
     */
    const RequestMethod = {
        DELETE: 'DELETE',
        GET:    'GET',
        POST:   'POST',
        PUT:    'PUT'
    };

    module.exports = RequestMethod; 
}());
