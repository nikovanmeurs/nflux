/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
module.exports = (function (undefined) {

    const
        AppKernel       = require('../core/AppKernel'),
        createClass     = require('../util/createClass');

    var AbstractService;

    /**
     * Defines several functions services should implement
     * @constructor
     */
    AbstractService = createClass({

        /**
         * Registers the service with the AppKernel
         */
        construct: function () {
            AppKernel.registerModule(this);
        },

        /**
         * Starts the service
         * @abstract
         * @throws Will throw an error if called
         */
        start: function () {
            throw new Error('Your service should implement AbstractService.start()');
        },

        /**
         * Stops the service
         * @abstract
         * @throws Will throw an error if called
         */
        stop: function () {
            throw new Error('Your service should implement AbstractService.stop()');
        }
    });

    return AbstractService;
}());