/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {
    const
        _                   = require('lodash'),
        ServiceContainer    = require('./container/ServiceContainer'),
        ModuleEvent         = require('../event/ModuleEvent');

    var AppKernel,
        _containers,
        _modules,
        _activeModules;

    /**
     * Calls AppKernel.startModule upon request
     * @param {String} moduleName
     */
    function handleModuleStart (moduleName) {
        try {
            AppKernel.startModule(moduleName);
            ModuleEvent.emit(ModuleEvent.STARTED, [moduleName]);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * Calls AppKernel.stopModule upon request
     * @param {String} moduleName
     */
    function handleModuleStop (moduleName) {
        try {
            AppKernel.stopModule(moduleName)
            ModuleEvent.emit(ModuleEvent.STOPPED, [moduleName]);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * The AppKernel singleton object is used to manage registered modules
     * @class
     */
    AppKernel = {

        /**
         * Initializes storage objects and event listeners
         * @returns {AppKernel}
         */
        construct: function () {
            _modules = {};
            _activeModules = [];
            _containers = {};

            this.createContainer('default');

            ModuleEvent
                .on(ModuleEvent.START, handleModuleStart)
                .on(ModuleEvent.STOP, handleModuleStop);

            return this;
        },

        /**
         * Creates a ServiceContainer with name containerName
         * @param {String} containerName
         * @returns {AppKernel}
         */
        createContainer: function (containerName) {

            var container,
                message;

            if (!containerName.length) {
                throw new Error('You dit not provide a container name');
            }

            _containers[containerName] = new ServiceContainer();

            return this;
        },


        /**
         * Returns the service container with name containerName
         * @param {String} containerName
         * @returns {ServiceContainer}
         */
        getContainer: function (containerName) {

            containerName = containerName || 'default';

            if (!_containers[containerName]) {
                message = 'Container {name} does not exist';
                throw new Error(message.replace(/\{name\}/g, containerName));
            }

            return _containers[containerName];
        },

        /**
         * Registers a module to be managed through the AppKernel
         * @param {AbstractService} module
         * @returns {AppKernel}
         */
        registerModule: function (module) {

            var name = module.name,
                message;

            _modules = _modules || {};

            if (_modules[module.name]) {
                message = 'A module with the name {name} already exists';
                throw new Error(message.replace(/\{name\}/g, name));
            }

            _modules[module.name] = module;

            return this;
        },

        /**
         * Starts a module if the module with name moduleName is registered with the AppKernel
         * @param {String} moduleName
         * @returns {AppKernel}
         * @throws Will throw an error if no module with name moduleName is registered
         * @throws Will throw an error if module with name moduleName is activated
         */
        startModule: function (moduleName) {

            var index,
                message;

            _activeModules = _activeModules || [];

            index = _.indexOf(_activeModules, moduleName);

            if (!_modules[moduleName]) {
                message = 'Module {name} is not registered';
                throw new Error(message.replace(/\{name\}/g, moduleName));
            }
            
            if (index === -1) {
                _modules[moduleName].start(this);
            } else {
                message = 'Module {name} is already active';
                throw new Error(message.replace(/\{name\}/g, moduleName));
            }

            return this;
        },

        /**
         * Stops a module if the module with name moduleName is registered with the AppKernel
         * @param {String} moduleName
         * @returns {AppKernel}
         * @throws Will throw an error if no module with name moduleName is registered
         * @throws Will throw an error if module with name moduleName is not activated
         */
        stopModule: function (moduleName) {

            var index,
                message;

            _activeModules = _activeModules || [];

            index = _.indexOf(_activeModules, moduleName);

            if (!_modules[moduleName]) {
                message = 'Module {name} is not registered';
                throw new Error(message.replace(/\{name\}/g, moduleName));
            }

            if (index !== -1) {
                _modules[moduleName].stop();
                _activeModules.splice(index, 1);
            } else {
                message = 'Module {name} is not active';
                throw new Error(message.replace(/\{name\}/g, moduleName));
            }

            return this;
        }
    };

    module.exports = AppKernel;
}());