/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {

    const
        createEvent = require('../util/createEvent');

    var RouteEvent;

    /**
     * Used to request and process page changes
     */
    RouteEvent = createEvent(
        [
            'route-add',
            'route-added',
            'route-match',
            'route-matched',
            'route-route',
            'route-routed',
            'route-generate'
        ]
    );

    module.exports = RouteEvent;
}());
