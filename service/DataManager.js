(function () {
    const
        _               = require('lodash'),
        immstruct       = require('immstruct'),
        { pluralize }   = require('inflect'),
        createClass     = require('../util/createClass'),
        AbstractService = require('./AbstractService');

    var DataManager,
        _instance,
        _data;

    DataManager = createClass(AbstractService, {

        construct: function () {
            _data = immstruct({});
        },

        name: 'data-manager',

        /**
         * Creates an immutable dataset
         * @param  {String} entityName  should be singlular
         * @param  {*}      data        can be anything, preferably an empty Object or Array
         * @return {Reference}
         */
        createDataset: function (entityName, data) {

            var datasetName,
                reference;

            datasetName = pluralize(entityName);

            _data.cursor().merge({ [datasetName]: data });

            reference = _data.reference(datasetName);

            return reference;
        },

        /**
         * Creates and instantiates a Store for entity entityName
         * @param  {String}     entityName     Should be singular
         * @param  {EventClass} EventClass     Should be constructed with createEvent
         * @return {Store}
         */
        createStore: function (entityName, EventClass) {

            var reference,
                suppressChanged,
                Store;

            reference = _instance.createDataset(entityName, []);
            suppressChanged = false;

            /**
             * @constructor
             */
            Store = {

                /**
                 * Adds an entity to the Store
                 * @param {Object} entity
                 * @returns {Store}
                 */
                add: function (entity) {

                    getCursor().update(function (cursor) {
                        return cursor.push(immstruct(entity).cursor());
                    });

                    if (!suppressChanged) {
                        triggerChanged();
                    }

                    return Store;
                },

                /**
                 * Adds multiple entities to the Store
                 * @param {Array} entities
                 * @returns {Store}
                 */
                addAll: function (entities) {

                    suppressChanged = true;

                    _.each(entities, Store.add);

                    suppressChanged = false;

                    triggerChanged();

                    return Store;
                },

                /**
                 * Returns all entities for which predicate is truthy
                 * @param  {Function|Object} predicate
                 * @return {Array}
                 */
                find: function (predicate) {

                    return _.where(getEntities(), predicate);
                },

                /**
                 * Returns all entities
                 * @return {Array}
                 */
                findAll: function () {

                    return getEntities();
                },

                /**
                 * Returns the first entity for which predicate is truthy
                 * @param {Function|Object} predicate
                 * @return {Object}
                 */
                findOne: function (predicate) {

                    return _.find(getEntities(), predicate);
                },

                /**
                 * Returns the entity with id = id
                 * @param  {Number|String} id
                 * @return {Object}
                 */
                findOneById: function (id) {

                    return Store.findOne({ id: id });
                },

                /**
                 * Removes entity from the store
                 * @param  {Object} entity
                 */
                remove: function (entity) {

                    var index = _.findIndex(getEntities(), entity);

                    if (-1 !== index) {
                        getCursor().delete(index);
                    }

                    triggerChanged();

                    return Store;
                },

                /**
                 * Replaces a single entity in the Store
                 * @param {Number|String} id
                 * @param {Object} newEntity
                 * @return Store
                 */
                replace: function (id, newEntity) {

                    var index = _.findIndex(getEntities(), { id: id });

                    getCursor().set(index, function (cursor) {
                        return immstruct(newEntity).cursor();
                    });

                    triggerChanged();
                    triggerListed();

                    return Store;
                },

                /**
                 * Replaces all entities in the Store
                 * @param {Array} entities
                 * @return Store
                 */
                replaceAll: function (entities) {

                    getCursor().clear();

                    Store.addAll(entities);

                    triggerChanged();
                    triggerListed();

                    return Store;
                },

                /**
                 * Modifies an entity according to the diff
                 * @param {Number} id
                 * @param {Update} update
                 * @returns Store
                 */
                update: function (id, datum) {

                    var index;

                    index = _.findIndex(getEntities(), { id: id });

                    getCursor().update(function (cursor) {
                        return cursor.merge(immstruct(datum).cursor());
                    });

                    return Store;
                },
            };

            // Event handlers, triggers and other private functions
            // Declared as function statements,
            // classes consuming the Store don't have anything to do with these

            /**
             * Returns a cursor for the store's dataset
             * @return {Cursor}
             * @see http://omniscientjs.github.io/api/02-immstruct-api-reference/
             */
            function getCursor () {
                return reference.cursor();
            }

            /**
             * Returns a JavaScript representation of the current dataset
             * @return {*}
             */
            function getEntities () {
                return getCursor().toJS();
            }

            /**
             * Triggers EventClass.CHANGE
             * @param {Object} entity
             */
            function triggerChanged () {

                EventClass.trigger(EventClass.CHANGED, Store.findAll());
            }

            /**
             * Triggers EventClass.LISTED
             * @see Store.findAll
             */
            function triggerListed () {

                // Passes all entities as event data
                EventClass.trigger(EventClass.LISTED, Store.findAll());
            }

            // Event binding
            EventClass
                // .on(EventClass.ADD, handleAdd)
                .on(EventClass.CREATE, Store.add)
                .on(EventClass.LIST, triggerListed)
                .on(EventClass.UPDATE, Store.update)
                .on(EventClass.REMOVE, Store.remove);

            if (EventClass.SET) {
                EventClass.on(EventClass.SET, Store.replaceAll);
            }

            return Store;
        },

        /**
         * Returns a cursor to the root of the aggregated dataset
         * @return {Cursor}
         * @see http://omniscientjs.github.io/api/02-immstruct-api-reference/
         */
        getCursor : function () {

            return _data.cursor();
        },

        on: function (eventName, callback) {
            _data.on(eventName, callback);

            return this;
        },

        start: function () {},
        stop: function () {}
    });

    _instance = new DataManager();

    module.exports = _instance;

}());