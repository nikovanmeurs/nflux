/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function () {
    var superFn;

    /**
     * Used to retrieve a function with name functionName from a superClass's prototype object
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/getPrototypeOf|Object.getPrototypeOf()}
     * @param {String} functionName
     * @returns {*}
     */
    superFn = function (functionName) {

        var prototype = Object.getPrototypeOf(Object.getPrototypeOf(this)),
            args;

        args = [].slice.call(arguments, 1);

        if (prototype.hasOwnProperty(functionName)) {
            return prototype[functionName].apply(this, args);
        }
    };

    module.exports = superFn;
}());
