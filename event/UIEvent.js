/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function (undefined) {

    const
        createEvent = require('../util/createEvent');

    var UIEvent;

    /**
     * Events triggered by various UI components
     */
    UIEvent = createEvent(
        [
            'ui-click-outside'
        ]
    );

    module.exports = UIEvent;
}());
