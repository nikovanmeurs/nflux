(function (undefined) {

    const
        AppKernel           = require('./core/AppKernel').construct(),
        ContainerAware      = require('./core/container/ContainerAware'),
        ServiceContainer    = require('./core/container/ServiceContainer'),
        JsonApi             = require('./core/http/JsonApi'),
        ModuleEvent         = require('./event/ModuleEvent'),
        RouteEvent          = require('./event/RouteEvent'),
        UIEvent             = require('./event/UIEvent'),
        DataManager         = require('./service/DataManager'),
        Router              = require('./service/Router'),
        createClass         = require('./util/createClass'),
        createEvent         = require('./util/createEvent'),
        createService       = require('./util/createService'),
        createStore         = require('./util/createStore'),
        createView          = require('./util/createView'),
        stringFromTemplate  = require('./util/stringFromTemplate');

    var Nflux;

    Nflux = {
        'core': {
            'container': {
                'ContainerAware': ContainerAware,
                'ServiceContainer': ServiceContainer
            },
            'http': {
                'JsonApi': JsonApi
            }
        },
        'event': {
            'ModuleEvent': ModuleEvent,
            'RouteEvent': RouteEvent,
            'UIEvent': UIEvent
        },
        'service': {
            'DataManager': DataManager,
            'Router': Router,
        },
        'util': {
            'stringFromTemplate': stringFromTemplate
        },
        'createClass': createClass,
        'createEvent': createEvent,
        'createService': createService,
        'createStore': createStore,
        'createView': createView,
        'kernel': AppKernel,
    };

    if (module.exports) {
        module.exports = Nflux;
    }

    if (window !== undefined) {
        window.nflux = Nflux;
    }
}());