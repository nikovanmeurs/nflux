/**
 * @author Niko van Meurs <niko@starapple.nl>
 */
(function () {
    const
        EventEmitter    = require('eventemitter3'),
        _               = require('lodash');

    var createEvent;

    /**
     * Creates an EventClass
     * @param {Array} eventNames,
     * @param {function} [triggerFunction]
     * @returns {Function}
     * @see createKey
     */
    createEvent = function (eventNames, triggerFunction) {

        var props = {};

        _.each(eventNames, function (eventName) {
            props[createKey(eventName)] = eventName;
        });

        // add a defaul triggerFunction if none was passed
        if (!triggerFunction || 'function' !== typeof triggerFunction) {
            triggerFunction = function () {

                console.log(arguments);
                this.emit.apply(this, [].slice.call(arguments, 0));
            };
        }

        props.trigger = triggerFunction;

        return _.merge({}, EventEmitter.prototype, props);
    };

    /**
     * Transforms a camelCase event name into all caps separated by underscores
     * @param {String} eventName
     * @returns {String}
     */
    function createKey (eventName) {
        var parts = eventName.split('-');

        return parts.slice(1).join('_').toUpperCase();
    }

    module.exports = createEvent;
}());
